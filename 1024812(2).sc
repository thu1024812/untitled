import scala.util.Random

//Ch5
def  groupByFirstChar(strs:List[String])={
  strs.groupBy(str=>str.head)
}
groupByFirstChar(List("Mark","Mike","Tom","Hi"))

def  groupByLength(strs:List[String])={
  strs.groupBy(str=>str.length==3)
}
groupByLength(List("Mark","Mike","Tom","Hi"))

def groupByIsOdd(nums:List[Int])={
  nums.groupBy(num=>num%2==1)
}
groupByIsOdd(List(1,2,3))

def sortByKey(kvs:List[(Int,String)])={
  kvs.sortBy(kv=>kv._1)
}
sortByKey(List(2->"c",1->"d",4->"a",3->"b"))

def sortByValue(kvs:List[(Int,String)])={
  kvs.sortBy(kv=>kv._2)
}
sortByValue(List(2->"c",1->"d",4->"a",3->"b"))

val list=List(2->"c",1->"d",4->"a",3->"b")
list.sortBy(kv=>{
  println(kv)
  kv._1
})

//Shuffle洗牌
//import在上面
val rnd=Random
rnd.nextInt()
(1 to 10)
(1 to 10).toList
List(1 to 10) // 錯的！！
val nums=(1 to 52).toList
nums.sortBy(i=>rnd.nextInt)
//取後不放回
nums.sortBy(i=>rnd.nextInt).take(10)

def shuffle(nums:List[Int])={

}


List(10,20,30,40).reduce((acc,x)=>{
  println("print acc="+", "+"x="+x)
  acc+x
})
def sum(nums:List[Int])={
    nums.reduce((acc,x)=>acc+x)
}

List(10,20,30,40).reduce((acc,x)=>acc+x)
List(10,20,30,40).reduce(_+_)
List(10,20,30,40).map(_+1)


List(1,2,3,4).reduce((acc,x)=>{
  println("print acc="+", "+"x="+x)
  acc*x
})


List("Hello","I","am","Mark").reduce((acc,x)=>{
  acc+" "+x
})
List("Hello","I","am","Mark").mkString(" ")
def mkString(strs:List[String])={
  strs.reduce((acc,str)=>acc+" "+str)
}

List(10,20,30,40).foldLeft(0)((acc,x)=>{
  println("fold acc="+acc+", "+"x="+x)
  acc+x
})
List(10,20,30,40).map(_+1)

List(10,20,30,40).foldLeft(1)((acc,x)=>{
  println("fold acc="+acc+", "+"x="+x)
  acc*x
})
List(10,20,30,40).map(_+1)


//必考題：累加
def accumulator(nums:List[Int])={
  nums.foldLeft(List(0))((acc,x)=>{
    println(acc)
    acc:+(acc.last+x)

  }).tail  //只要尾巴不要頭
}
accumulator(List(1,2,3,4,5))


//Word Count 字彙出現幾次
def wordCount(words:List[String])={

}
val words=List("Apple","Banana","Apple","Cherry","Cherry","Apple")
words.groupBy(v=>v).mapValues(_.length)
words.foldLeft(Map.empty[String,Int])((acc,x)=>{
  acc.get
})

"Hi Scala".split(" ")