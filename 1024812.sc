val t2="a"->1
t2
t2._1
t2._2

val tuple2_1=1->"Java"

val map1:Map[String,Int]=Map("x"->24,"y"->25,"z"->26)
map1("x")
map1.contains("a")
map1.contains("x")
map1+("x"->24)
map1+("x"->24,"a"->1)
map1-("y")
map1-("x","z")
map1+(("a"->1))
map1.keys
map1.values

map1.mapValues(v=>v+100)
map1.map(kv=>kv._1->(kv._2+100))
map1.filterKeys(k=>k=="a")
map1.filterKeys(k=>k=="x")
map1.filter(kv=>kv._1=="x")

val ms=Map("Mark"->1,"Tom"->2,"Hi"->3)
ms.filterKeys(str=>str.length<3)
ms("Mark")

List(1,2,3).filter(_>2)
List("Mark","Tom","Hi").filter(_.length<3)






